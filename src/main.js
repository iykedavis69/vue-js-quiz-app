import Vue from 'vue'
import App from './App.vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faFontAwesome)

Vue.use(BootstrapVue)
Vue.component('font-awesome-icon', FontAwesomeIcon)

library.add(faFontAwesome)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
